import penaltyblog as pb

#print all the countries that have league/s in the system.
print("List of leagues: ", pb.footballdata.list_countries())



#Asking from the user a country, season and divison.
wanted_league = input("Enter a league name:")

wanted_season = input("Enter a wanted season:")

wanted_division = input("Enter a wanted division (0 - 1st league, 1 - 2nd league...):")


#Taking the input and bring the games in the wanted league and year.
data = pb.footballdata.fetch_data(wanted_league, int(wanted_season), int(wanted_division))


#print(data[["HomeTeam"]])#list_of_teams = []#i = 0#while i < 10:#    list_of_teams[i] = data[["HomeTeam"]](i)#print(list_of_teams)    #print(data[["HomeTeam"], (10)])    #not_organised_data = pd.read_csv('data', header=None, skiprows = [0])    #print(not_organised_data)


#Creating a table with all the teams and attacking/defending points.
pois = pb.poisson.PoissonGoalsModel(
    data["FTHG"], data["FTAG"], data["HomeTeam"], data["AwayTeam"])
pois.fit()



#Asking for a home team and a away team.
home_team = input("Enter a home team:")
away_team = input("Enter a away team:")


#The algorithem is predicting the odds of every teams to win.
probs = pois.predict(home_team, away_team)



#printing the results.
print("The chances that " , home_team , " will win is: " , round(probs.home_draw_away[0] * 100, 2) , "%")
print("The chances that " , away_team , " will win is: " , round(probs.home_draw_away[2] * 100) , "%")
print("The chances that the match will end in a draw is: " , round(probs.home_draw_away[1] * 100) ,"%")