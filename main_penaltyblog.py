import penaltyblog as pb

pb.footballdata.list_countries()


#Asking the the wanted season from the user and return a list of games in the season and the length of that list.
def choose_season_div():
    wanted_season = input("Enter a wanted season:")
    wanted_division = input("Enter a wanted division (0 - 1st league, 1 - 2nd league...):")

    chosen_data = pb.footballdata.fetch_data("england", int(wanted_season), int(wanted_division))
    lentgh = len(chosen_data)
    return chosen_data, lentgh


#Asking the user what he want to do in this program and return his answear.
def select_option():
    print("What do you want?")
    print("1. Predict the game's score.")
    print("2. See the previous game's score.")
    choose_option = int(input())
    return choose_option


#Asking him what kind of games he wants to get.
def routing_for_previous():
    print("What do you want?")
    print("1. Team last games.")
    print("2. Team last home games")
    print("3. Team last away games")
    print("4. last game between 2 teams")
    chosen_input = input()

    if chosen_input == str(1):
        return 1
    elif chosen_input == str(2):
        return 2
    elif chosen_input == str(3):
        return 3
    elif chosen_input == str(4):
        return 4
    else:
        print("wrong input")


#Doing what he asked for.
def doing_the_chosen_option(option_number, wanted_data):
    data = wanted_data

    if option_number == 1:
        wanted_team = input("Enter a wanted team:")
        i = 1

        while i <= len:
            if wanted_team == data.loc[len - i, 'HomeTeam'] \
                    or wanted_team == data.loc[len - i, 'AwayTeam']:
                print(data[["Date", "HomeTeam", "AwayTeam", "FTHG", "FTAG"]].loc[[len - i]])
            i += 1
    elif option_number == 2:
        wanted_team = input("Enter a wanted team:")
        i = 1

        while i <= len:
            if wanted_team == data.loc[len - i, 'HomeTeam']:
                print(data[["Date", "HomeTeam", "AwayTeam", "FTHG", "FTAG"]].loc[[len - i]])
            i += 1
    elif option_number == 3:
        wanted_team = input("Enter a wanted team:")
        i = 1

        while i <= len:
            if wanted_team == data.loc[len - i, 'AwayTeam']:
                print(data[["Date", "HomeTeam", "AwayTeam", "FTHG", "FTAG"]].loc[[len - i]])
            i += 1
    elif option_number == 4:
        wanted_team_1 = input("Enter a wanted team:")
        wanted_team_2 = input("Enter a 2nd wanted team:")
        i = 1

        while i <= len:
            if (wanted_team_1 == data.loc[len - i, 'HomeTeam']
                and wanted_team_2 == data.loc[len - i, 'AwayTeam']) \
                    or (wanted_team_2 == data.loc[len - i, 'HomeTeam']
                        and wanted_team_1 == data.loc[len - i, 'AwayTeam']):
                print(data[["Date", "HomeTeam", "AwayTeam", "FTHG", "FTAG"]].loc[[len - i]])
            i += 1


#Doing a table of all the teams in the chosen league and their offense/defense.
def table_of_teams(wanted_data):
    pois = pb.poisson.PoissonGoalsModel(
        wanted_data["FTHG"], wanted_data["FTAG"], wanted_data["HomeTeam"], wanted_data["AwayTeam"])
    pois.fit()
    return pois


#Calculating the teams probs to win according to the table.
def calculating_the_odds(table_data):
    home_team = input("Enter a home team:")
    away_team = input("Enter a away team:")

    tot = table_of_teams(table_data)
    probs = tot.predict(home_team, away_team)
    return probs, home_team, away_team


#Printing the chances that the previous function calculated.
def output_the_probs(probs, home_team, away_team):
    print("The chances that ", home_team, " will win is: ", round(probs.home_draw_away[0] * 100, 2), "%")
    print("The chances that the match will end in a draw is: ", round(probs.home_draw_away[1] * 100, 2), "%")
    print("The chances that ", away_team, " will win is: ", round(probs.home_draw_away[2] * 100, 2), "%")


if __name__ == '__main__':
    chosen_season_div = choose_season_div()
    data = chosen_season_div[0]
    len = chosen_season_div[1]

    selected = select_option()
    if selected == 1:
        output_data = calculating_the_odds(data)
        output_the_probs(output_data[0], output_data[1], output_data[2])
    elif selected == int(2):
        chosen_option = routing_for_previous()
        doing_the_chosen_option(chosen_option, data)
    else:
        print("Wrong input")
