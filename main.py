import csv
import pandas as pd
import xgboost as xgb
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from IPython.display import display


data = pd.read_csv('data/data_18-19.csv')

#display(data.head())



def home_team_win_rate():
    """
    calculate win percentage of home teams.
    :return: number of matches, number of features, number of matches won by home teams and win rate of home teams.
    """
    n_matches = data.shape[0]
    n_features = data.shape[1] - 1
    n_homewins = len(data[data.FTR == 'H'])
    win_rate = (float(n_homewins)/(n_matches)*100)
    print(f"Total number of matches:{n_matches}")
    print(f"Number of features:{n_features}")
    print(f"Number of matches won by home team:{n_homewins}")
    print(f"Win rate of home team:{round(win_rate, 2)}%")





from pandas.plotting import scatter_matrix







scatter_matrix(data[['HTGD','ATGD','HTP','ATP','DiffFormPts','DiffLP']], figsize=(10,10))
































#print(data.to_string())
home_team_win_rate()




